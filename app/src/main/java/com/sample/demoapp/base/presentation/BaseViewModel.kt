package com.sample.demoapp.base.presentation

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.sample.demoapp.base.data.model.DialogBoxModel
import com.sample.demoapp.base.domain.BaseUseCase
import com.sample.demoapp.base.navigation.BaseRouteDestination
import com.sample.demoapp.base.navigation.BaseRouteNavHost
import com.sample.demoapp.base.navigation.Event
import com.sample.demoapp.base.navigation.defaultNavOptions
import kotlinx.coroutines.Job

open class BaseViewModel : ViewModel() {

    var useCases = emptyArray<BaseUseCase>()

    var job: Job? = null

    private val dialogBoxMutableLiveData = MutableLiveData<DialogBoxModel>()
    val dialogBoxLiveData: LiveData<DialogBoxModel> get() = dialogBoxMutableLiveData

    private val loadingMutableLiveData = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> get() = loadingMutableLiveData

    private val navigationMutableLiveData = MutableLiveData<Event<NavController.() -> Any>>()
    val navigationLiveData: LiveData<Event<NavController.() -> Any>> get() = navigationMutableLiveData

    fun addUseCases(useCases: Array<BaseUseCase>) {
        this.useCases = useCases
    }

    fun navigateTo(route: BaseRouteNavHost, args: Bundle?) {
        withNavController { navigate(route.graph, args, defaultNavOptions) }
    }

    fun navigateTo(route: BaseRouteDestination, args: Bundle?, clearStack: Boolean) {
        when {
            route is BaseRouteDestination.Back -> withNavController { popBackStack() }
            clearStack -> withNavController { popBackStack(route.destination, false) }
            else -> withNavController { navigate(route.destination, args, defaultNavOptions) }
        }
    }

    private fun withNavController(block: NavController.() -> Any) {
        navigationMutableLiveData.postValue(Event(block))
    }

    fun startOrPauseLoading(isLoading: Boolean) {
        loadingMutableLiveData.postValue(isLoading)
    }

    fun openDialogBoxModel(dialogBoxModel: DialogBoxModel) {
        dialogBoxMutableLiveData.postValue(dialogBoxModel)
    }

    fun onBackPressed() {}

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}