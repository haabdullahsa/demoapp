package com.sample.demoapp.base.data.model

open class BaseRequest(
    val apiKey: String? = null,
    val page: Int? = null
)