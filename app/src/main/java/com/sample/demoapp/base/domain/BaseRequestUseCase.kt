package com.sample.demoapp.base.domain

import com.sample.demoapp.base.data.model.BaseResponse
import com.sample.demoapp.base.data.model.DialogBoxModel
import com.sample.demoapp.base.data.model.DialogButton
import com.sample.demoapp.base.data.network.ResponseObserver
import com.sample.demoapp.base.data.network.Status
import com.sample.demoapp.base.util.Constants.OK
import retrofit2.Response
import timber.log.Timber

abstract class BaseRequestUseCase<T : BaseResponse, Params> :
    BaseUseCase() {

    abstract suspend fun networkCall(params: Params): Response<T>

    suspend fun execute(
        responseObserver: ResponseObserver<T>,
        params: Params
    ) {
        try {
            responseObserver.loading(Status.Loading)
            val response = networkCall(params)
            if (response.isSuccessful) {
                response.body()?.let { body ->
                    responseObserver.success(
                        Status.Success(body, body.dialogBoxes?.get(0))
                    )
                }
            } else {
                responseObserver.error(
                    Status.Error(
                        DialogBoxModel(
                            DialogBoxModel.TYPE_ERROR,
                            response.code().toString(),
                            response.message(),
                            dialogButton = DialogButton(OK)
                        )
                    )
                )
            }
        } catch (e: Exception) {
            responseObserver.error(
                Status.Error(
                    DialogBoxModel(
                        null,
                        null,
                        message = e.message ?: e.toString(),
                        dialogButton = DialogButton(OK)
                    )
                )
            )
        }
    }

    suspend fun executeWithPaging(
        params: Params
    ): Response<T> {
        return networkCall(params)
    }

    private fun <T : BaseResponse> error(dialogBoxModel: DialogBoxModel): Status {
        Timber.d(dialogBoxModel.message)
        return Status.Error(dialogBoxModel)
    }

}