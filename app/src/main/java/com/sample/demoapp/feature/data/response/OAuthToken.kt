package com.sample.demoapp.feature.data.response

import com.google.gson.annotations.SerializedName
import com.sample.demoapp.base.data.model.BaseResponse

class OAuthToken(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("token_type")
    val tokenType: String,
) : BaseResponse() {

    fun getAuthorization(): String {
        return "$tokenType $accessToken"
    }

    data class ViewEntity(
        val accessToken: String?,
        val tokenType: String?,
        val authorization: String?
    )

    fun toViewEntity() = ViewEntity(
        accessToken,
        tokenType,
        getAuthorization()
    )
}