package com.sample.demoapp.feature.presentation.list.adapter.model

import android.os.Parcelable
import com.sample.demoapp.base.presentation.list.model.BaseListItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListItemModel(
    val id: String?,
    val userName: String?,
    val photoUrl: String?,
    val message: String?
) : BaseListItem<ListItemModel>, Parcelable {
    override fun areItemsTheSame(oldItem: ListItemModel, newItem: ListItemModel) =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: ListItemModel, newItem: ListItemModel) =
        oldItem.id == newItem.id && oldItem.message == newItem.message && oldItem.photoUrl == newItem.photoUrl
}