package com.sample.demoapp.feature.data.response

import com.google.gson.annotations.SerializedName

data class Tweet(
    @SerializedName("id")
    val id: String,
    @SerializedName("text")
    val message: String,
    @SerializedName("user")
    val user: User
) {

    data class ViewEntity(
        val id: String?,
        val message: String?,
        val user: User.ViewEntity?
    )

    fun toViewEntity() = ViewEntity(
        id,
        message,
        user.toViewEntity()
    )
}
