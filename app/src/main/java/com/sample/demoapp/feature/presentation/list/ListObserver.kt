package com.sample.demoapp.feature.presentation.list

import com.sample.demoapp.base.data.network.ResponseObserver
import com.sample.demoapp.base.data.network.Status
import com.sample.demoapp.feature.data.response.DataResponse
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel

class ListObserver(private val viewModel: ListViewModel) :
    ResponseObserver<DataResponse>(viewModel) {

    override fun onSuccess(statusData: Status.Success<DataResponse>) {
        viewModel.onSuccessLoadedList(statusData.data.toViewEntity().list.map {
            ListItemModel(
                it.id,
                it.user?.name,
                it.user?.profileImageUrl,
                it.message
            )
        })
    }

    override fun onError(statusData: Status.Error) {
        statusData.dialogBox?.let { viewModel.onError(it) }
    }
}