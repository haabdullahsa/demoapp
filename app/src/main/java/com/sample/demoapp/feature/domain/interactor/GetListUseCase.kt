package com.sample.demoapp.feature.domain.interactor

import com.sample.demoapp.base.domain.BaseRequestUseCase
import com.sample.demoapp.feature.data.response.DataResponse
import com.sample.demoapp.feature.domain.DemoRepository

class GetListUseCase constructor(private val repository: DemoRepository) :
    BaseRequestUseCase<DataResponse, GetListUseCase.Params>() {

    override suspend fun networkCall(params: Params) = params.run {
        repository.getTweetList(token)
    }

    class Params(
        val token: String
    )
}