package com.sample.demoapp.feature.utils.navigation

import androidx.annotation.IdRes
import com.sample.demoapp.R
import com.sample.demoapp.base.navigation.BaseRouteDestination

// For Fragments to navigate
sealed class PagesDestination(@IdRes destination: Int) : BaseRouteDestination(destination) {
    object ListPage : PagesDestination(R.id.listFragment)
    object DetailPage : PagesDestination(R.id.action_listFragment_to_detailFragment)
}