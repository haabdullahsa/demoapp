package com.sample.demoapp.feature.presentation.detail

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sample.demoapp.base.presentation.BaseViewModel
import com.sample.demoapp.feature.presentation.detail.DetailFragment.Companion.ARG_ITEM
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel

class DetailViewModel : BaseViewModel() {

    private val itemMutableLiveData = MutableLiveData<ListItemModel>()
    val itemLiveData: LiveData<ListItemModel> get() = itemMutableLiveData

    fun getArguments(bundle: Bundle?) {
        bundle?.let {
            itemMutableLiveData.value = it.getParcelable(ARG_ITEM)
        }
    }

}