package com.sample.demoapp.feature.data.repository

import com.sample.demoapp.feature.data.datasource.remote.RemoteDataSource
import com.sample.demoapp.feature.data.response.DataResponse
import com.sample.demoapp.feature.data.response.OAuthToken
import com.sample.demoapp.feature.domain.DemoRepository
import retrofit2.Response

class DemoRepositoryImpl(private val remoteDataSource: RemoteDataSource) : DemoRepository {
    override suspend fun getTweetList(token: String): Response<DataResponse> =
        remoteDataSource.getTweetList(token)

    override suspend fun authorize(token: String, grantType: String): Response<OAuthToken> =
        remoteDataSource.authorize(token, grantType)
}