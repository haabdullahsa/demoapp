package com.sample.demoapp.feature.presentation.list

import androidx.core.os.bundleOf
import androidx.databinding.ObservableField
import com.sample.demoapp.base.data.model.DialogBoxModel
import com.sample.demoapp.base.presentation.BaseViewModel
import com.sample.demoapp.feature.domain.interactor.AuthenticateUseCase
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import com.sample.demoapp.feature.presentation.detail.DetailFragment.Companion.ARG_ITEM
import com.sample.demoapp.feature.presentation.list.adapter.ListAdapter
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel
import com.sample.demoapp.feature.utils.navigation.PagesDestination
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber


class ListViewModel(
    private val getListUseCase: GetListUseCase,
    private val authenticateUseCase: AuthenticateUseCase
) : BaseViewModel() {
    val adapterObservable: ObservableField<ListAdapter> = ObservableField()
    val listObservable: ObservableField<List<ListItemModel>> = ObservableField()

    private val adapter: ListAdapter by lazy {
        ListAdapter {
            navigateTo(
                PagesDestination.DetailPage, bundleOf(
                    ARG_ITEM to it
                ), false
            )
        }
    }

    fun getList(token: String) {
        job = CoroutineScope(Dispatchers.IO).launch {
            getListUseCase.execute(ListObserver(this@ListViewModel), GetListUseCase.Params(token))
        }
    }

    fun authorize(token: String, grantType: String) {
        job = CoroutineScope(Dispatchers.IO).launch {
            authenticateUseCase.execute(
                AuthorizeObserver(this@ListViewModel),
                AuthenticateUseCase.Params(token, grantType)
            )
        }
    }

    fun onSuccessLoadedList(list: List<ListItemModel>?) = list?.let {
        adapterObservable.set(adapter)
        listObservable.set(it)
    }

    fun onError(dialogBoxModel: DialogBoxModel?) {
        dialogBoxModel?.let { openDialogBoxModel(it) }
    }

    fun onSuccessAuthorization(token: String?) = token?.let {
        Timber.d(it)
        getList(it)
    }
}