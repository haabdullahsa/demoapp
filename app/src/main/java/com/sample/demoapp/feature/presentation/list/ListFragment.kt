package com.sample.demoapp.feature.presentation.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.sample.demoapp.BuildConfig
import com.sample.demoapp.R
import com.sample.demoapp.base.presentation.BaseFragment
import com.sample.demoapp.databinding.FragmentListBinding
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.Credentials
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment : BaseFragment<ListViewModel, FragmentListBinding>() {

    @Inject
    lateinit var listViewModelFactory: ListViewModelFactory

    override fun provideLayoutResId() = R.layout.fragment_list

    override fun provideViewModel() =
        ViewModelProvider(this, listViewModelFactory).get(ListViewModel::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinding().btnLogin.setOnClickListener {
            getViewModel().authorize(
                Credentials.basic(
                    BuildConfig.CONSUMER_KEY,
                    BuildConfig.CONSUMER_SECRET,
                ), "client_credentials"
            )
        }
    }

    override fun bindViewModel(dataBinding: FragmentListBinding) {
        dataBinding.viewModel = getViewModel()
    }
}