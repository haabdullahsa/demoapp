package com.sample.demoapp.feature.presentation.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.sample.demoapp.R
import com.sample.demoapp.base.presentation.BaseFragment
import com.sample.demoapp.databinding.FragmentDetailBinding
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : BaseFragment<DetailViewModel, FragmentDetailBinding>() {

    companion object {
        const val ARG_ITEM = "KEY_LIST_ITEM"
    }

    @Inject
    lateinit var viewModelFactory: DetailViewModelFactory

    private var item: ListItemModel? = null

    override fun provideLayoutResId() = R.layout.fragment_detail

    override fun provideViewModel() =
        ViewModelProvider(this, viewModelFactory).get(DetailViewModel::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewModel().getArguments(arguments)
        getViewModel().let {
            it.itemLiveData.observe(viewLifecycleOwner, { item ->
                getBinding().data = item
            })
        }
    }

    override fun bindViewModel(dataBinding: FragmentDetailBinding) {
        dataBinding.data = item
    }
}