package com.sample.demoapp.feature.presentation.list

import com.sample.demoapp.base.data.network.ResponseObserver
import com.sample.demoapp.base.data.network.Status
import com.sample.demoapp.feature.data.response.OAuthToken

class AuthorizeObserver(private val viewModel: ListViewModel) :
    ResponseObserver<OAuthToken>(viewModel) {

    override fun onSuccess(statusData: Status.Success<OAuthToken>) {
        viewModel.onSuccessAuthorization(statusData.data.toViewEntity().authorization)
    }

    override fun onError(statusData: Status.Error) {
        viewModel.onError(statusData.dialogBox)
    }
}