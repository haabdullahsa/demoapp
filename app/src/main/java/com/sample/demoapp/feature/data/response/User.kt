package com.sample.demoapp.feature.data.response

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("name")
    val name: String,
    @SerializedName("profile_image_url")
    val profileImageUrl: String
) {
    data class ViewEntity(
        val name: String?,
        val profileImageUrl: String?
    )

    fun toViewEntity() = ViewEntity(
        name,
        profileImageUrl
    )
}