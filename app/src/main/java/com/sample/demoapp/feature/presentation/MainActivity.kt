package com.sample.demoapp.feature.presentation

import androidx.lifecycle.ViewModelProvider
import com.sample.demoapp.R
import com.sample.demoapp.base.presentation.BaseActivity
import com.sample.demoapp.base.util.LoadingProgressDialog
import com.sample.demoapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    @Inject
    lateinit var viewModelFactory: MainViewModelFactory

    private val progressDialog: LoadingProgressDialog by lazy {
        LoadingProgressDialog(this, lifecycle)
    }

    override fun provideLayoutResId() = R.layout.activity_main

    override fun provideViewModel() =
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

    override fun hideLoading() {
        progressDialog.dismiss()
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun bindViewModel(dataBinding: ActivityMainBinding) {
        // DO NOTHING
    }
}