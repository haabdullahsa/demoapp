package com.sample.demoapp.feature.domain

import com.sample.demoapp.feature.data.response.DataResponse
import com.sample.demoapp.feature.data.response.OAuthToken
import retrofit2.Response

interface DemoRepository {
    suspend fun getTweetList(token: String): Response<DataResponse>

    suspend fun authorize(token: String, grantType: String): Response<OAuthToken>
}