package com.sample.demoapp.feature.presentation

import com.sample.demoapp.base.domain.BaseUseCase
import com.sample.demoapp.base.presentation.BaseViewModelFactory
import javax.inject.Inject

class MainViewModelFactory @Inject constructor() : BaseViewModelFactory<MainViewModel>() {
    override fun provideViewModel() = MainViewModel()

    override fun provideUseCases() = arrayOf<BaseUseCase>()
}