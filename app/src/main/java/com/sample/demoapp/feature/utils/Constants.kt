package com.sample.demoapp.feature.utils

object Constants {
    const val BASE_URL = "https://api.twitter.com/"
    const val GET_TWEETS = "1.1/search/tweets.json"
    const val TWITTER_ACCESS_TOKEN_URL = "oauth2/token"
    const val CALLBACK_URL = "twittersdk://"
}