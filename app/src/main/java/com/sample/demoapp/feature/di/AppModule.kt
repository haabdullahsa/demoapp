package com.sample.demoapp.feature.di

import com.sample.demoapp.feature.data.datasource.remote.ApiService
import com.sample.demoapp.feature.data.datasource.remote.RemoteDataSource
import com.sample.demoapp.feature.data.repository.DemoRepositoryImpl
import com.sample.demoapp.feature.domain.DemoRepository
import com.sample.demoapp.feature.domain.interactor.AuthenticateUseCase
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRemoteDataSource(apiService: ApiService): RemoteDataSource =
        RemoteDataSource(apiService)

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: RemoteDataSource): DemoRepository =
        DemoRepositoryImpl(remoteDataSource)

    @Singleton
    @Provides
    fun provideGetMovieListUseCase(repository: DemoRepository): GetListUseCase =
        GetListUseCase(repository)

    @Singleton
    @Provides
    fun provideAuthenticateUseCase(repository: DemoRepository): AuthenticateUseCase =
        AuthenticateUseCase(repository)
}