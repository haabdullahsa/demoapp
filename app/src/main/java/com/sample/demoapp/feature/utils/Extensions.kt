package com.sample.demoapp.feature.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sample.demoapp.feature.presentation.list.adapter.ListAdapter
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel

@BindingAdapter("setAdapter", "submitList")
fun RecyclerView.loadList(adapter: ListAdapter?, list: List<ListItemModel>?) {
    this.adapter = adapter
    adapter?.submitList(list)
}

@BindingAdapter("app:srcUrl")
fun ImageView.setUrl(url: String?) {
    Glide.with(this).load(url)
        .into(this)
}