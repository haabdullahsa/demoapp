package com.sample.demoapp.feature.data.response

import com.google.gson.annotations.SerializedName
import com.sample.demoapp.base.data.model.BaseResponse

class DataResponse(
    @SerializedName("statuses")
    val list: List<Tweet>
) : BaseResponse() {

    data class ViewEntity(
        val list: List<Tweet.ViewEntity>
    )

    fun toViewEntity() = ViewEntity(
        list.map { it.toViewEntity() }
    )
}