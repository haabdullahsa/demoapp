package com.sample.demoapp.feature.domain.interactor

import com.sample.demoapp.base.domain.BaseRequestUseCase
import com.sample.demoapp.feature.data.response.OAuthToken
import com.sample.demoapp.feature.domain.DemoRepository

class AuthenticateUseCase constructor(private val repository: DemoRepository) :
    BaseRequestUseCase<OAuthToken, AuthenticateUseCase.Params>() {

    override suspend fun networkCall(params: Params) = params.run {
        repository.authorize(params.token, grantType)
    }

    class Params(
        val token: String,
        val grantType: String
    )
}