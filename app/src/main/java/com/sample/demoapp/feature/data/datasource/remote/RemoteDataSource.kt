package com.sample.demoapp.feature.data.datasource.remote

class RemoteDataSource(private val apiService: ApiService) {
    suspend fun authorize(authorization: String, grantType: String) =
        apiService.postCredentials(authorization, grantType)

    suspend fun getTweetList(authorization: String) = apiService.getTweetList(authorization, "a")
}