package com.sample.demoapp.feature.presentation.list

import com.sample.demoapp.base.domain.BaseUseCase
import com.sample.demoapp.base.presentation.BaseViewModelFactory
import com.sample.demoapp.feature.domain.interactor.AuthenticateUseCase
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import javax.inject.Inject


class ListViewModelFactory @Inject constructor(
    val getListUseCase: GetListUseCase,
    val authenticateUseCase: AuthenticateUseCase
) :
    BaseViewModelFactory<ListViewModel>() {

    override fun provideViewModel() = ListViewModel(getListUseCase, authenticateUseCase)

    override fun provideUseCases() = arrayOf<BaseUseCase>(getListUseCase, authenticateUseCase)
}