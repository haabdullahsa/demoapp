package com.sample.demoapp.feature.presentation.list.adapter

import com.sample.demoapp.R
import com.sample.demoapp.base.presentation.list.BaseRecyclerViewAdapter
import com.sample.demoapp.databinding.ItemListBinding
import com.sample.demoapp.feature.presentation.list.adapter.model.ListItemModel

class ListAdapter(onItemClicked: (ListItemModel) -> Unit) :
    BaseRecyclerViewAdapter<ListItemModel, ItemListBinding>(R.layout.item_list, onItemClicked) {

    override fun bind(binding: ItemListBinding, item: ListItemModel) {
        binding.data = item
    }
}