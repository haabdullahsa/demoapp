package com.sample.demoapp.feature.data.datasource.remote

import com.sample.demoapp.feature.data.response.DataResponse
import com.sample.demoapp.feature.data.response.OAuthToken
import com.sample.demoapp.feature.utils.Constants.GET_TWEETS
import com.sample.demoapp.feature.utils.Constants.TWITTER_ACCESS_TOKEN_URL
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @FormUrlEncoded
    @POST(TWITTER_ACCESS_TOKEN_URL)
    suspend fun postCredentials(
        @Header("Authorization") authorization: String,
        @Field("grant_type") grantType: String
    ): Response<OAuthToken>

    @GET(GET_TWEETS)
    suspend fun getTweetList(
        @Header("Authorization") authorization: String,
        @Query("q") hashTag: String
    ): Response<DataResponse>
}