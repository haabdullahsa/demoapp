package com.sample.demoapp.feature.presentation.detail

import com.sample.demoapp.base.domain.BaseUseCase
import com.sample.demoapp.base.presentation.BaseViewModelFactory
import javax.inject.Inject

class DetailViewModelFactory @Inject constructor() : BaseViewModelFactory<DetailViewModel>() {

    override fun provideViewModel() = DetailViewModel()

    override fun provideUseCases() = arrayOf<BaseUseCase>()
}