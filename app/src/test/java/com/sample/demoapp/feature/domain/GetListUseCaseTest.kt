package com.sample.demoapp.feature.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sample.demoapp.CoroutineTestRule
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class GetListUseCaseTest {

    @MockK
    lateinit var repository: DemoRepository

    private lateinit var getListUseCase: GetListUseCase

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        getListUseCase = GetListUseCase(repository)
    }

    @Test
    fun `given params, when networkCall called, then verify getTweetList from repository`() {
        coroutineTestRule.dispatcher.runBlockingTest {
            // Given
            val params = GetListUseCase.Params(
                ArgumentMatchers.anyString()
            )
            coEvery { repository.getTweetList(any()) } returns mockk()

            // When
            getListUseCase.networkCall(params)

            // Then
            coVerify {
                repository.getTweetList(any())
            }
        }
    }

}