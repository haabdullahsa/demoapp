package com.sample.demoapp.feature.list

import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import com.sample.demoapp.CoroutineTestRule
import com.sample.demoapp.base.BaseViewModelTest
import com.sample.demoapp.base.data.model.DialogBoxModel
import com.sample.demoapp.feature.domain.interactor.AuthenticateUseCase
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import com.sample.demoapp.feature.presentation.list.ListObserver
import com.sample.demoapp.feature.presentation.list.ListViewModel
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class ListViewModelTest : BaseViewModelTest() {

    @MockK
    lateinit var getListUseCase: GetListUseCase

    @MockK
    lateinit var authenticateUseCase: AuthenticateUseCase

    private lateinit var viewModel: ListViewModel

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun init() {
        MockKAnnotations.init(this)
        viewModel = ListViewModel(getListUseCase, authenticateUseCase)
    }

    @Test
    fun `given nothing, when getList called, then verify executing getListUseCase`() {
        coroutineTestRule.dispatcher.runBlockingTest {
            // Given
            coEvery {
                getListUseCase.execute(
                    any<ListObserver>(),
                    any()
                )
            } returns Unit

            // When
            viewModel.getList("")

            // Then
            coVerify {
                getListUseCase.execute(
                    any<ListObserver>(),
                    any()
                )
            }
        }
    }

    @Test
    fun `given null data, when onSuccessLoadedList called, then check to not trigger observable fields`() {
        // Given
        val data = null
        val mockObservable = Mockito.mock(ObservableField::class.java)

        // When
        viewModel.onSuccessLoadedList(data)

        //Then
        Mockito.verify(mockObservable, Mockito.never()).set(any())
    }

    @Test
    fun `given dialogBoxModel, when onError called, then check to open dialog`() {
        // Given
        val data = mockk<DialogBoxModel>()
        val mockDialogBoxLiveData = spyk<Observer<DialogBoxModel>>()
        viewModel.dialogBoxLiveData.observeForever(mockDialogBoxLiveData)

        // When
        viewModel.onError(data)

        //Then
        verify {
            mockDialogBoxLiveData.onChanged(capture(slot()))
        }
        assert(viewModel.dialogBoxLiveData.value != null)
    }
}