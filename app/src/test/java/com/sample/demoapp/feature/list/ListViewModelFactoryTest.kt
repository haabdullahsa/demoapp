package com.sample.demoapp.feature.list

import com.sample.demoapp.feature.domain.interactor.AuthenticateUseCase
import com.sample.demoapp.feature.domain.interactor.GetListUseCase
import com.sample.demoapp.feature.presentation.list.ListViewModel
import com.sample.demoapp.feature.presentation.list.ListViewModelFactory
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ListViewModelFactoryTest {
    @MockK
    lateinit var getListUseCase: GetListUseCase

    @MockK
    lateinit var authenticateUseCase: AuthenticateUseCase

    private lateinit var viewModelFactory: ListViewModelFactory

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModelFactory = ListViewModelFactory(getListUseCase, authenticateUseCase)
    }

    @Test
    fun testViewModel() {
        // Given

        // When
        viewModelFactory.provideViewModel()

        // Then
        assertEquals(getListUseCase, viewModelFactory.getListUseCase)
    }

    @Test
    fun testUseCases() {
        // Given

        // When
        val viewModel = viewModelFactory.create(ListViewModel::class.java)

        // Then
        Assert.assertNotNull(viewModel)
        Assert.assertTrue(listOf(viewModel.useCases).isNotEmpty())
    }
}