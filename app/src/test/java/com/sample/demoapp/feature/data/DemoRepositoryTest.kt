package com.sample.demoapp.feature.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sample.demoapp.CoroutineTestRule
import com.sample.demoapp.feature.data.datasource.remote.RemoteDataSource
import com.sample.demoapp.feature.data.repository.DemoRepositoryImpl
import com.sample.demoapp.feature.domain.DemoRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DemoRepositoryTest {

    @MockK
    lateinit var remoteDataSource: RemoteDataSource

    private lateinit var repository: DemoRepository

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        repository = DemoRepositoryImpl(remoteDataSource)
    }

    @Test
    fun `given params, when getMovieList called, then verify getMovieListResponse from remoteDataSource`() {
        coroutineTestRule.dispatcher.runBlockingTest {
            // Given
            coEvery { remoteDataSource.getTweetList(any()) } returns mockk()

            // When
            repository.getTweetList("")

            // Then
            coVerify {
                remoteDataSource.getTweetList(any())
            }
        }
    }
}